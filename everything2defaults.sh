#!/bin/sh

mydir="$(cd "$(dirname "$(realpath "$0")")" && pwd)"
registrar="${mydir}/prog2defaults.sh"
progslist="${mydir}/defaults.list"

if [ ! -e "$registrar" ]; then
    echo "** ERROR: Programs registrar (${registrar}) doesn't exist"
    exit 1
fi

if [ ! -e "$progslist" ]; then
    echo "** ERROR: List of default programs (${progslist}) doesn't exist"
    exit 1
fi

xargs -a "$progslist" -d'\n' "$registrar"
