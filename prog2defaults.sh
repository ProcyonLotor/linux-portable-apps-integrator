#!/bin/sh

BIN_DIR="${HOME}/.local/bin"
ICO_DIR="${HOME}/.local/share/icons"
APP_DIR="${HOME}/.local/share/applications"

[ ! -e "$BIN_DIR" ] && mkdir -p "$BIN_DIR"
[ ! -e "$ICO_DIR" ] && mkdir -p "$ICO_DIR"
[ ! -e "$APP_DIR" ] && mkdir -p "$APP_DIR"

for prog in "$@"; do
    echo ""
    echo "Working on \"$prog\"..."

    prog_exec_path="$(realpath "$prog")"
    prog_name="$(basename "$prog_exec_path" .sh)"
    prog_dir="$(dirname "$prog_exec_path")"
    if [ -e "$prog_exec_path" ]; then
        ln -sf "$prog_exec_path" "${BIN_DIR}/${prog_name}"
    else
        echo "** ERROR: Program (${prog_exec_path}) doesn't exist, skipping..."
        continue
    fi

    prog_icon="${prog_name}.png"
    prog_icon_path="${prog_dir}/${prog_icon}"
    if [ -e "$prog_icon_path" ]; then
        ln -sf "$prog_icon_path" "${ICO_DIR}/${prog_icon}"
    else
        echo "** WARNING: Icon file (${prog_icon_path}) doesn't exist, continuing without icon..."
    fi

    prog_desk="${prog_name}.desktop"
    prog_desk_path="${prog_dir}/${prog_desk}"
    if [ -e "$prog_desk_path" ]; then
        ln -sf "$prog_desk_path" "${APP_DIR}/${prog_desk}"
    else
        echo "** WARNING: Desktop file (${prog_desk_path}) doesn't exist, continuing without desktop entry..."
    fi

    echo "done;"
done

echo ""
echo "Done."
