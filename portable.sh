#!/bin/sh

#
# Wrapper script for making apps standalone (portable)
#
realname="$(realpath "$0")"
filename="$(basename "$realname" .sh)"
filepath="$(dirname "$realname")"

export HOME="${filepath}/userdata/${USER}"
[ ! -e "$HOME" ] && mkdir -p "$HOME"

export _JAVA_OPTIONS="${_JAVA_OPTIONS} -Duser.home=\"${HOME}\""

cd "$filepath"
"${filepath}/${filename}" "$@"
